# pag2019_demo_sklearn

Demo given Jan. 14th 2019 at Plant and Animal Genome conference in San Diego.
Txt files contain positive (cross over containing regions) and negative cases from the tomato genome. Each line lists one genomic region, described by chromosome and start/end position; this is followed by the feature values (descriptors such as GC content, occurence of specific motifs, etc).For more background information, see Demirci et al. Plant Journal 2018 (https://onlinelibrary.wiley.com/doi/abs/10.1111/tpj.13979).